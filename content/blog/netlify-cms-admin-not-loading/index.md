---
title: Netlify cms admin not loading
description: Netlify cms admin not loading
date: 2022-07-18T21:26:00.969Z
featuredimage: netlifycms-admin-trailing-slash.jpg
---
As in the correct answer [here](https://answers.netlify.com/t/cms-js-404s-and-wont-load-the-admin-side/6173/4), there has to be a trailing slash after 'admin': 'mywonderfulnetlifysite.netlify.app/admin***/***'