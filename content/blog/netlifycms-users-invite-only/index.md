---
title: Netlifycms - users invite only
description: Steps to allow users only via invite
date: 2022-07-17T10:42:10.565Z
---
Steps to achieve that:



* /settings/identity -> invite only

![](netlifycms-invite-only.jpg)

* /identity -> invite new user

  ![](netlifycms-invite-users.jpg)

![]()

That should do it?