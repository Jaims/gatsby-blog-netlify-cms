---
title: 'netlifycms "Failed to load entry: API_ERROR: [object Object]"'
description: In GitLab version 15.0, support for OAuth tokens with no expiration
  date was removed (gitlab issue 16). This meant that using our Git Gateway
  service started to have a 2-hour time limit when configured with GitLab and
  OAuth. This was a breaking change for many of our users...
date: 2022-07-17T13:59:11.866Z
---
Found [here](https://answers.netlify.com/t/how-to-mitigate-changes-to-gitlab-version-15-0-support-for-oauth-tokens/56613)

In GitLab version 15.0, support for OAuth tokens with no expiration date was removed ([gitlab issue 16](https://gitlab.com/gitlab-org/gitlab/-/issues/340848#note_949500475)). This meant that using our Git Gateway service started to have a 2-hour time limit when configured with GitLab and OAuth. This was a breaking change for many of our users.

You can mitigate this problem by using a Personal Access Token. The steps are:

* log into the GitLab UI and generate a token (User Settings > Access Tokens)
* create the token with the scopes you need. For NetlifyCMS users those are: api, read_api, read_repository, & write_repository
* copy & save that value to the Netlify UI: Site settings > Identity > Git Gateway

We are going to be evaluating using OAuth and refresh tokens with GitLab in the near future.



![gitlab access tokens die in 2h](netlifycms-api-error-again.jpg "gitlab access tokens die in 2h")