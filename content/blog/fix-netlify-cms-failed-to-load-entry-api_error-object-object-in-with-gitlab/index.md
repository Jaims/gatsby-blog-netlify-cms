---
title: Fix Netlify cms failed to load entry api_error object object in with Gitlab
description: "For some reason netlifycms fails to load objects, displaying a
  popup that reads: 'to load entry api_error <object> <object'>. "
date: 2022-07-16T20:33:26.102Z
featuredimage: netlifycms-api-error.jpg
---
All I had to do was to regenerate the gitlab api access token in the project settings, identity, git gateway.

It remains a mistery why this token needed to be regenerated. Does it have to be regenerated after a deployment. Get out of here!

Read this: <https://dev.to/agiksetiawan/fix-netlify-cms-failed-to-load-entry-apierror-object-object-in-with-gitlab-20o7>