import React from "react";
import Layout from "@components/layout"

export default function Contact({ data, location }) {
  return (
    <Layout location={location}>
      <div>
        <h1>Contact form</h1>
        <p>
          You can use this form to reach out to me 😎
        </p>
        <form
          name="contact"
          method="POST"
          data-netlify="true"
          className="bg-white shadow-md rounded px-8 pt-6 pb-8 mb-4">
          <div className="flex items-center justify-between">
            <label>Name
              <input type="text" name="name"
                className="shadow appearance-none border border-red-500 rounded w-full py-2 px-3 text-gray-700 mb-3 leading-tight focus:outline-none focus:shadow-outline"
              /></label>
          </div>
          <div className="flex items-center justify-between">
            <label>Email
              <input type="email" name="email"
                className="shadow appearance-none border border-red-500 rounded w-full py-2 px-3 text-gray-700 mb-3 leading-tight focus:outline-none focus:shadow-outline" />
            </label>
          </div>
          <div className="flex items-center justify-between">
            <label>Email
              <textarea name="description"
                cols="40"
                className="shadow appearance-none border border-red-500 rounded w-full py-2 px-3 text-gray-700 mb-3 leading-tight focus:outline-none focus:shadow-outline" />
            </label>
          </div>
          <div className="flex items-center justify-between">
            <button
              type="submit"
              className="bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded focus:outline-none focus:shadow-outline"
            >Send</button>
          </div>
        </form>
      </div>
    </Layout>
  )
}