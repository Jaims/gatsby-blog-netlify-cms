import React from "react"

export default function Footer({ location }) {
  return (
    <footer className="w-full p-6 text-center border-t border-indigo-500 pin-b">
      <p>
        Created by Jaime Seuma (borrowed from <a href="https://taimoorsattar.dev">Taimoor Sattar</a>) &bull;
        &copy; 2021
      </p>
      <p><small>Borrowed because flipping netlify cms won't install with conflicting reactjs versions 😢</small></p>
    </footer>
  )
}
